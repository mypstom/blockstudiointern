
let deltaTime = 0;
let initSpeed = 0.2;
let maxSpeed = 1.5;
let speed;

let isSpeedUp = false;

let tl1, tl2, tl3, tl4;

$(document).ready(function(){
    speed = initSpeed;

    //左眼
    tl1 = new TimelineMax({repeat: -1, repeatDelay: 0, delay: 0.2});
    tl1.set(".tear", {opacity: 0, scale: 0, x: "20%", y: "-20%"});
    tl1.to(".tear", {scale: 0.8 , opacity: 1, duration: 0.3});
    tl1.to(".tear", {y: "+=300%",scale: 0, opacity: 0, duration: 1});

    tl2 = new TimelineMax({repeat: -1, repeatDelay: 0, delay: 0.85});
    tl2.set(".tear2", {opacity: 0, scale: 0, x: "20%", y: "-20%"});
    tl2.to(".tear2", {scale: 0.8 , opacity: 1, duration: 0.3});
    tl2.to(".tear2", {y: "+=300%",scale: 0, opacity: 0, duration: 1});

    //右眼
    tl3 = new TimelineMax({repeat: -1, repeatDelay: 0});
    tl3.set(".tear3", {opacity: 0, scale: 0, x: "-90%", y: "-70%"});
    tl3.to(".tear3", {scale: 0.5 , opacity: 1, duration: 0.3});
    tl3.to(".tear3", {y: "+=150%",scale: 0, opacity: 0, duration: 1});

    tl4 = new TimelineMax({repeat: -1, repeatDelay: 0, delay: 0.65});
    tl4.set(".tear4", {opacity: 0, scale: 0, x: "-90%", y: "-70%"});
    tl4.to(".tear4", {scale: 0.5 , opacity: 1, duration: 0.3});
    tl4.to(".tear4", {y: "+=150%",scale: 0, opacity: 0, duration: 1});

    
    tl1.timeScale(0.1);
    tl2.timeScale(0.1);
    tl3.timeScale(0.1);
    tl4.timeScale(0.1);
    // TweenMax.to(".tear2", 0.5,{yoyo: 1, repeat: -1, x: 10});
    // TweenMax.to(".tear3", 0.5,{yoyo: 1, repeat: -1, x: 15});

    // $(".molly-container").on("mouseover", function(){
    //     isSpeedUp = true;
    //     tl1.timeScale(2);
    //     tl2.timeScale(2);
    //     tl3.timeScale(2);
    //     tl4.timeScale(2);
    // })

    // $(".molly-container").on("mouseleave", function(){
    //     isSpeedUp = false;
    //     tl1.timeScale(0.1);
    //     tl2.timeScale(0.1);
    //     tl3.timeScale(0.1);
    //     tl4.timeScale(0.1);
    // })

    // $(".molly-container").on("touchstart", function(){
    //     isSpeedUp = true;
    //     tl1.timeScale(2);
    //     tl2.timeScale(2);
    //     tl3.timeScale(2);
    //     tl4.timeScale(2);
    // })

    // $(".molly-container").on("touchend", function(){
    //     isSpeedUp = false;
    //     tl1.timeScale(0.1);
    //     tl2.timeScale(0.1);
    //     tl3.timeScale(0.1);
    //     tl4.timeScale(0.1);
    // })

    $("body").on("click", function(){
        isSpeedUp = !isSpeedUp;
        if(isSpeedUp){
            tl1.timeScale(2);
            tl2.timeScale(2);
            tl3.timeScale(2);
            tl4.timeScale(2);
        }else{
            tl1.timeScale(0.1);
            tl2.timeScale(0.1);
            tl3.timeScale(0.1);
            tl4.timeScale(0.1);
        }
    })

    $(window).blur(function(){
        isSpeedUp = false;
        tl1.timeScale(0.1);
        tl2.timeScale(0.1);
        tl3.timeScale(0.1);
        tl4.timeScale(0.1);
    });
    

    tick();    
});



let pigOffset = 0;

function tick(){
    
    
    if(isSpeedUp){
        speed+=0.01;
        if(speed >= maxSpeed){
            speed = maxSpeed;
        }

        pigOffset+=0.5;
    }else{
        speed-=0.05;
        if(speed <= initSpeed){
            speed = initSpeed;
        }
    }

    deltaTime += speed;
    if(deltaTime >= 14){
        deltaTime = 0;
    }
    $(".m-coin").css("left", deltaTime*10*0.53 -16.5  + "%");
    $(".m-coin").css("top", deltaTime*deltaTime*0.13 - 10 + "%");

    $(".m-pi").css("left", Math.sin(pigOffset)*10+ .362*1024 +20 + "px");
    $(".m-sigle").css("left", Math.sin(pigOffset)*10+ 0*1024 +20 + "px");

    requestAnimationFrame(tick);
}


