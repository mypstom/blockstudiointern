
let deltaTime = 0;
let initSpeed = 0.03;
let maxSpeed = 0.8;
let speed;

let isSpeedUp = false;


$(document).ready(function(){
    speed = initSpeed;

    // TweenMax.to(".tear2", 0.5,{yoyo: 1, repeat: -1, x: 10});
    // TweenMax.to(".tear3", 0.5,{yoyo: 1, repeat: -1, x: 15});

    // $(".molly-container").on("mouseenter", function(){
    //     isSpeedUp = true;
    // })

    // $(".molly-container").on("mouseleave", function(){
    //     isSpeedUp = false;
    // })

    // $(".molly-container").on("touchstart", function(){
    //     isSpeedUp = true;
    // })

    // $(".molly-container").on("touchend", function(){
    //     isSpeedUp = false;
    // })

    $("body").on("click", function(){
        isSpeedUp = !isSpeedUp;
    })

    $(window).blur(function(){
        isSpeedUp = false;
    });
    
    fishGroup = $(".fish");

    tick();    
});


let fishGroup = [];
let pigOffset = 0;
let delta = 0;

function tick(){
    
    for(let i=0; i<fishGroup.length; i++){
        $(fishGroup[i]).css("left",  Math.sin(i/2.0+delta)*120 +"px");
        $(fishGroup[i]).css("top", i*120+"px");
    }

    
    if(isSpeedUp){
        speed+=0.01;
        if(speed >= maxSpeed){
            speed = maxSpeed;
        }

    }else{
        speed-=0.05;
        if(speed <= initSpeed){
            speed = initSpeed;
        }
    }

    delta += speed;
    
    requestAnimationFrame(tick);
}


