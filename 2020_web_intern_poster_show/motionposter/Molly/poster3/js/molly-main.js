
let deltaTime = 0;
let initSpeed = 3;
let maxSpeed = 10;
let speed = 10;

let isSpeedUp = false;


$(document).ready(function(){
    speed = initSpeed;

    $(".molly-container").on("click", function(){
        isSpeedUp = !isSpeedUp;
        $(".chicken").toggleClass("active");
    })

    $(".molly-container").on("touchstart", function(){
        isSpeedUp = !isSpeedUp;
        $(".chicken").toggleClass("active");
    })

    $(window).blur(function(){
        isSpeedUp = false;
    });

    tick();    
});

let delta = 0;

function tick(){

    $(".m-ahh").css("transform", `rotate(65deg) translateX(${ -Math.round(delta)}px)`);
    
    if(delta > 620){
        delta = 0;
    }

    if(isSpeedUp){
        speed = maxSpeed;

    }else{
        speed = initSpeed;
    }

    delta += speed;
    
    requestAnimationFrame(tick);
}


