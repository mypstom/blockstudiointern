
let scene, camera, controls, clock, mixer=null;
let renderScene, bloomPass, composer;
let sphereSparkleState = false;
let blackAni;

let params = {
    bloomStrength: 0.2,
    bloomThreshold: 0,
    bloomRadius: 1
};

let heart;
let isSpeedUp = false;
let maxSpeed = 1, minSpeed = 0.01;


$(document).ready(()=>{
    setup();
    animate();
});


function setup(){
    scene = new THREE.Scene();
    clock = new THREE.Clock();
    
    // camera = new THREE.PerspectiveCamera( 75, $('.container').width()/ $('.container').height(), 0.1, 10000 );
    let width = 7.68, height = 10.24;
    camera = new THREE.OrthographicCamera( width / - 2, width / 2, height / 2, height / - 2, 0.1, 10000 );
    // camera.rotation.y = -10;
    camera.position.set(0, 0, 15);
    
    if(isMobile()){
        renderer = new THREE.WebGLRenderer({alpha:true, antialias:true});
    }else{
        renderer = new THREE.WebGLRenderer({alpha:true, antialias:true, precision:"highp"});
    }

    renderer.setSize( $('.container').width(), $('.container').height() );
    // renderer.setClearColor( 0x060810, 0); 
    renderer.setPixelRatio( window.devicePixelRatio );
    $('.container').append( renderer.domElement )

    // controls = new THREE.OrbitControls(camera, renderer.domElement);

    // var helper = new THREE.GridHelper( 1000, 1000, 0x4411AA, 0x4411AA );
    // scene.add( helper );


    //環境光照
    var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.5 );
    scene.add( directionalLight );
    window.dLight = directionalLight;
    var light = new THREE.AmbientLight( 0xFFFFFF, 1); // soft white light
    scene.add( light );
    
    
    loadModels();
    addPostPass();


    $( ".container" ).on("click", function( event ) {
        isSpeedUp = !isSpeedUp;
    });

    $( ".container" ).on("mouseleave", function( event ) {
        isSpeedUp = false;
    });

    $(window).blur(function(){
        console.log("blur");
        isSpeedUp = false;
    });

}


function loadModels(){
    addParticle();
    gltfLoaderBG();
    gltfLoaderHeart();
}



function gltfLoaderHeart(){
    // Instantiate a loader
    var loader = new THREE.GLTFLoader();

    // Load a glTF resource
    loader.load(
        // resource URL
        './star_v2.gltf',
        // called when the resource is loaded
        function ( gltf ) {
            gltf.scene.position.set(0, -1, 12);
            gltf.scene.scale.set(1, 1, 1);
            heart = gltf.scene;
            
            let matY1 = new THREE.MeshStandardMaterial({
                color: 0xFFFFFF,
                side: THREE.DoubleSide,
                map: new THREE.TextureLoader().load( "./starTex.jpg")
            });

            heart.children[0].traverse( function( node ) {
                if(node.material){
                    // node.material = matY1;
                }
            });
            scene.add( heart );
            
            window.heart = heart;
        },
        // called while loading is progressing
        function ( xhr ) {
            console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
        },
        // called when loading has errors
        function ( error ) {
            console.log( 'An error happened' );
        }
    );
}

function gltfLoaderBG(){
    // Instantiate a loader
    var loader = new THREE.GLTFLoader();

    // Load a glTF resource
    loader.load(
        // resource URL
        './tree.gltf',
        // called when the resource is loaded
        function ( gltf ) {
            gltf.scene.position.set(0, -1, 12);
            gltf.scene.scale.set(1, 1, 1);
            let object = gltf.scene;

            //樹木處理
            object.children[0].traverse( function( node ) {
                if(node.name == "樹"){
                    let count = 0;
                    node.traverse( function( leaf ) {
                        if(count % 2 ==0){
                            leaf.material = new THREE.MeshPhongMaterial({color: 0x3CB42F});
                        }else{
                            leaf.material = new THREE.MeshPhongMaterial({color: 0x5AE344});
                        }
                        count++;
                    });
                }
            });


            let path = "./assets/cube/Park2/";
            let format = '.jpg';
            let urls = [
                path + 'negx' + format, path + 'posx' + format,
                path + 'negy' + format, path + 'posy' + format,
                path + 'negz' + format, path + 'posz' + format
            ];
        
            let textureCube = new THREE.CubeTextureLoader().load( urls );
        
            textureCube.mapping = THREE.CubeRefractionMapping;
        
            let glassRefMatR = new THREE.MeshPhongMaterial( { 
                color: 0xEF3E4F,
                envMap: textureCube,
                reflectivity: 0.3,
                opacity: 0.8,
                transparent: true
            } );
            let glassRefMatG = new THREE.MeshPhongMaterial( { 
                color: 0xF7E527,
                envMap: textureCube,
                reflectivity: 0.3,
                opacity: 0.8,
                transparent: true
            } );
            let glassRefMatB = new THREE.MeshPhongMaterial( { 
                color: 0x47CAE5,
                envMap: textureCube,
                reflectivity: 0.3,
                opacity: 0.8,
                transparent: true
            } );

            let counter = 0;

            object.traverse( function( node ) {
                if(node.name == "left" || node.name == "right" || node.name == "down"){
                    node.material = new THREE.MeshPhongMaterial({
                        color: 0xFFE668,
                        // emissive: 0xFFE668,
                        // emissiveIntensity: 1
                    });
                }

                if(node.name.indexOf("球体") != -1 ){
                    console.log(node.name);
                    if(counter%3 == 0){
                        node.material = glassRefMatR;
                    }
                    if(counter%3 == 1){
                        node.material = glassRefMatG;
                    }
                    if(counter%3 == 2){
                        node.material = glassRefMatB;
                    }
                }
                counter++;

            });

            


            scene.add( object );
            window.bg = object;
        },
        // called while loading is progressing
        function ( xhr ) {
            console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
        },
        // called when loading has errors
        function ( error ) {
            console.log( 'An error happened' );
        }
    );
}




function followMouse(){

    var vector = new THREE.Vector3(mouse.x, mouse.y, 0.5);
    vector.unproject( camera );
    var dir = vector.sub( camera.position ).normalize();
    var distance = - camera.position.z / dir.z;
    var pos = camera.position.clone().add( dir.multiplyScalar( distance ) );
}

let speed = minSpeed;
let particleSys = null;

function animate(){
    if(window.heart){

        if(isSpeedUp && speed<maxSpeed){
            speed += 0.005;
            if(speed >= maxSpeed){
                speed = maxSpeed;
            }
        }else{
            speed -= 0.02;
            if(speed<= minSpeed){
                speed = minSpeed;
            }
        }

        if(bloomPass){
            if(isSpeedUp){
                bloomPass.strength = 0.2;
            }else{
                bloomPass.strength = 0;
            }
        }

        window.heart.rotation.y += speed;
    }

    if(particleSys){

        
        particleSys.geometry.vertices.forEach(particle => {
            if(particle.alive){
                particle.y -= particle.speed/10;
                if(particle.y <= -5){
                    particle.y = 5;
                }
            }
            
        })
        particleSys.geometry.verticesNeedUpdate = true;
    }
    renderer.render( scene, camera );
    composer.render();
    requestAnimationFrame( animate );
}


function addPostPass(){
    renderScene = new THREE.RenderPass(scene, camera);
    
    let effectFXAA = new THREE.ShaderPass(THREE.FXAAShader);
    effectFXAA.uniforms['resolution'].value.set(0.2 / window.innerWidth, 0.2 / window.innerHeight);
    
    let copyShader = new THREE.ShaderPass(THREE.CopyShader);
    copyShader.renderToScreen = true;

    let bloomPass = new THREE.UnrealBloomPass(new THREE.Vector2(window.innerWidth, window.innerHeight), params.bloomStrength, params.bloomRadius, params.bloomThreshold);
    bloomPass.renderToScreen = true;
    bloomPass.threshold = params.bloomThreshold;
    bloomPass.strength = params.bloomStrength;
    bloomPass.radius = params.bloomRadius;


    composer = new THREE.EffectComposer(renderer);
    
    composer.setSize($('.container').width(), $('.container').height());
    composer.addPass(renderScene);
    composer.addPass(effectFXAA);
    composer.addPass(bloomPass);
    composer.addPass(copyShader);
    
}


let particleCount = 100;
let particleDistance = 53;
let particleGeom;
let particleMaterial;



function addParticle(){
    
    particleGeom = new THREE.Geometry();

    particleMaterial = new THREE.PointsMaterial({
        color: 'rgb(255, 255, 255)',
        size:  Math.random()*30.0 +15,
        map: new THREE.TextureLoader().load('./snow.png'),
        transparent: true,
        emissive: "#FFFFFF",
        emissiveIntensity: 5
        // blending: THREE.AdditiveBlending,
    });

    
    for (let i=0; i<particleCount; i++) {

        //生成目標位置的隨機 2d 圓形區域
        let posX, posZ;
        posX = 20 * Math.random() -10;
        posZ = 50 * Math.random() -10;

        let particle = new THREE.Vector3(posX, 5, posZ);
        particle.speed = Math.random()+0.3;
        // particle.id = i;
        particleGeom.vertices.push(particle);
    }


    setInterval(function() {
        if(isSpeedUp){
            addOneHeart();
        }
    }, 50);

    particleSys = new THREE.Points(particleGeom, particleMaterial);
    particleSys.name = 'particleSys';
    particleSys.position.set(0, 0.65, 0);
    window.ps = particleSys;
    scene.add(particleSys);
}

let useIndex = 0;
function addOneHeart(){
    //一次調整一個點
    particleSys.geometry.vertices[useIndex].alive = true;
    particleSys.geometry.verticesNeedUpdate = true;
    useIndex++;
    useIndex = useIndex%particleCount;
}


