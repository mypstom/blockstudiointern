
let scene, camera, controls, clock, mixer=null;
let renderScene, bloomPass, composer;
let sphereSparkleState = false;
let blackAni;

let params = {
    bloomStrength: 10,
    bloomThreshold: 0,
    bloomRadius: 3
};

$(document).ready(()=>{
    setup();
    animate();
});

let raycaster = new THREE.Raycaster();
let mouse = new THREE.Vector2();



function setup(){
    scene = new THREE.Scene();
    clock = new THREE.Clock();
    
    // camera = new THREE.PerspectiveCamera( 75, $('.container').width()/ $('.container').height(), 0.1, 10000 );
    let width = 7.68, height = 10.24;
    camera = new THREE.OrthographicCamera( width / - 2, width / 2, height / 2, height / - 2, 1, 1000 );
    // camera.rotation.y = -10;
    camera.position.set(0, 0, 20);
    
    if(isMobile()){
        renderer = new THREE.WebGLRenderer({alpha:true, antialias:true});
    }else{
        renderer = new THREE.WebGLRenderer({alpha:true, antialias:true, precision:"highp"});
    }

    renderer.setSize( $('.container').width(), $('.container').height() );
    // renderer.setClearColor( 0x060810, 0); 
    renderer.setPixelRatio( window.devicePixelRatio );
    $('.container').append( renderer.domElement )

    // controls = new THREE.OrbitControls(camera, renderer.domElement);

    let geo = new THREE.SphereGeometry( 2.5, 32, 32 );
    let sphereMat = new THREE.MeshPhongMaterial( {color: 0xF28989} );
    let sphere = new THREE.Mesh( geo, sphereMat );
    sphere.position.set(0, -1, 0);
    sphere.layers.enable( 2 );
    scene.add(sphere);

    window.sphere = sphere;

    // var helper = new THREE.GridHelper( 1000, 1000, 0x4411AA, 0x4411AA );
    // scene.add( helper );


    //環境光照
    var directionalLight = new THREE.DirectionalLight( 0xffffff, 1 );
    scene.add( directionalLight );
    var light = new THREE.AmbientLight( 0xFFFFCB ); // soft white light
    scene.add( light );

    //滑鼠光照
    var mouseLight = new THREE.PointLight( 0xF37500, 0.5, 100);
    mouseLight.position.set( 0, 0, 10 );
    scene.add( mouseLight );
    window.mouseLight = mouseLight;



    blackAni = new TimelineMax();
    blackAni.to(".black", 0.01,{opacity:1});
    blackAni.to(".black", 0.01,{opacity:0}, "+=0.1");
    
    blackAni.to(".black", 0.01,{opacity:1}, "+=0.1");
    blackAni.to(".black", 0.01,{opacity:0}, "+=0.1");
    blackAni.to(".black", 0.01,{opacity:1}, "+=0.1");
    blackAni.to(".black", 0.01,{opacity:0}, "+=0.1");
    blackAni.to(".black", 0.01,{opacity:1}, "+=0.1");
    blackAni.to(".black", 0.01,{opacity:0}, "+=0.1");

    blackAni.pause();
    
    
    raycaster = new THREE.Raycaster();
    // raycaster.layers.set( 2 );
    renderer.domElement.addEventListener( 'click', raycast, false );


    loadModels();
    // addPostPass();

    $( ".container" ).mousemove(function( event ) {
        var msg = "Handler for .mousemove() called at ";
        msg += event.pageX + ", " + event.pageY;
        
        mouseLight.position.x = event.pageX.map(0, 768, -20, 20);
        mouseLight.position.y = -event.pageY.map(0, 1024, -10, 10);
    });
    

}


function loadModels(){

    var fbx_loader = new THREE.FBXLoader();
    fbx_loader.load('./ghost.fbx', function(object) {

        object.traverse( function( node ) {
            if( node.material ) {
                node.material.side = THREE.DoubleSide;
            }
        });

        object.scale.multiplyScalar(.025);    // 缩放模型大小
        object.position.set(-3, 0, 10);
        object.rotation.set(Math.PI, -Math.PI/2, Math.PI);

        mixer = new THREE.AnimationMixer(object);

        // mixers.push(object.mixer);
        let action = mixer.clipAction(object.animations[0]);
        action.setDuration(4).play();
        scene.add(object);
        object.name = "ghostAAA";
        window.ghost = object;
        blinkOnce();
        
        mixer.addEventListener( 'loop', function( e ) {
            blinkOnce();
        } );


        


    }, onProgress, onError);

    function onProgress(e){
        console.log(e);
    }

    function onError(e){
        console.log(e);
    }


}


function blinkOnce(){
    blackAni.play();
    blackAni.restart();
    setTimeout(() => {
        blackAni.play();
        blackAni.restart();
    }, 3000);
}

function followMouse(){

    var vector = new THREE.Vector3(mouse.x, mouse.y, 0.5);
    vector.unproject( camera );
    var dir = vector.sub( camera.position ).normalize();
    var distance = - camera.position.z / dir.z;
    var pos = camera.position.clone().add( dir.multiplyScalar( distance ) );
}



function animate(){
    if(mixer !== null){
        mixer.update(clock.getDelta());
    }

    renderer.render( scene, camera );
    requestAnimationFrame( animate );
}


function addPostPass(){
    renderScene = new THREE.RenderPass(scene, camera);
    
    let effectFXAA = new THREE.ShaderPass(THREE.FXAAShader);
    effectFXAA.uniforms['resolution'].value.set(0.2 / window.innerWidth, 0.2 / window.innerHeight);
    
    let copyShader = new THREE.ShaderPass(THREE.CopyShader);
    copyShader.renderToScreen = true;

    let bloomPass = new THREE.UnrealBloomPass(new THREE.Vector2(window.innerWidth, window.innerHeight), params.bloomStrength, params.bloomRadius, params.bloomThreshold);
    /* bloomPass.renderToScreen = true; */
    bloomPass.threshold = params.bloomThreshold;
    bloomPass.strength = params.bloomStrength;
    bloomPass.radius = params.bloomRadius;

    // glitchPass = new THREE.GlitchPass();


    composer = new THREE.EffectComposer(renderer);
    
    composer.setSize($('.container').width(), $('.container').height());
    composer.addPass(renderScene);
    composer.addPass(effectFXAA);
    composer.addPass(bloomPass);
    composer.addPass(copyShader);
    // composer.addPass(glitchPass);
}


let canClick = true;

function raycast ( e ) {
    

    //1. sets the mouse position with a coordinate system where the center
    //   of the screen is the origin
    mouse.x = ( e.screenX / window.innerWidth ) * 2 - 1;
    mouse.y = - ( e.screenY / window.innerHeight ) * 2 + 1;

    // console.log(e);
    

    //2. set the picking ray from the camera position and mouse coordinates
    raycaster.setFromCamera( mouse, camera );    

    //3. compute intersections
    var intersects = raycaster.intersectObjects( scene.children, true );

    if(canClick){
        canClick = false;
        $(".fire-target").addClass("emoji-firework");
        $(".fire-target").css("top", e.pageY+"px");
        $(".fire-target").css("left", e.pageX+"px");

        setTimeout(() => {
            canClick = true;
            $(".fire-target").removeClass("emoji-firework");
        }, 500);
    }

    for ( var i = 0; i < intersects.length; i++ ) {
        // console.log( intersects[ i ].object.name ); 
        

        
        if(intersects[i].object.name == "body" || true){
            console.log("煙火");
            
            if(canClick){
                canClick = false;
                $(".fire-target").addClass("emoji-firework");
                $(".fire-target").css("top", e.pageY+"px");
                $(".fire-target").css("left", e.pageX+"px");
    
                setTimeout(() => {
                    canClick = true;
                    $(".fire-target").removeClass("emoji-firework");
                }, 500);
            }
            
        }



        /*
            An intersection has the following properties :
                - object : intersected object (THREE.Mesh)
                - distance : distance from camera to intersection (number)
                - face : intersected face (THREE.Face3)
                - faceIndex : intersected face index (number)
                - point : intersection point (THREE.Vector3)
                - uv : intersection point in the object's UV coordinates (THREE.Vector2)
        */
    }

}


