
let scene, camera, controls, clock, mixer=null;
let renderScene, bloomPass, composer;
let sphereSparkleState = false;
let blackAni;

let params = {
    bloomStrength: 10,
    bloomThreshold: 0,
    bloomRadius: 10
};

let heart;
let isSpeedUp = false;
let maxSpeed = 1, minSpeed = 0.01;


$(document).ready(()=>{
    setup();
    animate();
});


function setup(){
    scene = new THREE.Scene();
    clock = new THREE.Clock();
    
    // camera = new THREE.PerspectiveCamera( 75, $('.container').width()/ $('.container').height(), 0.1, 10000 );
    let width = 7.68, height = 10.24;
    camera = new THREE.OrthographicCamera( width / - 2, width / 2, height / 2, height / - 2, 1, 10000 );
    // camera.rotation.y = -10;
    camera.position.set(0, 0, 15);
    
    if(isMobile()){
        renderer = new THREE.WebGLRenderer({alpha:true, antialias:true});
    }else{
        renderer = new THREE.WebGLRenderer({alpha:true, antialias:true, precision:"highp"});
    }

    renderer.setSize( $('.container').width(), $('.container').height() );
    // renderer.setClearColor( 0x060810, 0); 
    renderer.setPixelRatio( window.devicePixelRatio );
    $('.container').append( renderer.domElement )

    // controls = new THREE.OrbitControls(camera, renderer.domElement);

    // var helper = new THREE.GridHelper( 1000, 1000, 0x4411AA, 0x4411AA );
    // scene.add( helper );


    //環境光照
    var directionalLight = new THREE.DirectionalLight( 0xffffff, 1 );
    scene.add( directionalLight );
    var light = new THREE.AmbientLight( 0xFFFFFF ); // soft white light
    scene.add( light );
    
    
    loadModels();
    // addPostPass();

    $( ".container" ).mousemove(function( event ) {
        var msg = "Handler for .mousemove() called at ";
        msg += event.pageX + ", " + event.pageY;
    });


    $( "body" ).on("click", function( event ) {
        isSpeedUp = !isSpeedUp;
    });


    // $( "body" ).mousedown(function( event ) {
    //     console.log("down");
    //     isSpeedUp = true;
    // });

    // $( "body" ).mouseup(function( event ) {
    //     console.log("up");
    //     isSpeedUp = false;
    // });

    $(window).blur(function(){
        console.log("blur");
        isSpeedUp = false;
    });
    



}


function loadModels(){
    gltfLoaderBG();
    gltfLoaderHeart();
}



function gltfLoaderHeart(){
    // Instantiate a loader
    var loader = new THREE.GLTFLoader();

    // Load a glTF resource
    loader.load(
        // resource URL
        './heart.gltf',
        // called when the resource is loaded
        function ( gltf ) {
            gltf.scene.position.set(0, -4, 10);
            gltf.scene.scale.set(5, 5, 5);
            heart = gltf.scene;
            let matY1 = new THREE.MeshPhongMaterial({color: 0xFFD500});
            let matY2 = new THREE.MeshPhongMaterial({color: 0xFFB94F});
            let matH = new THREE.MeshPhongMaterial({color: 0xFF6B79});
            heart.children[0].traverse( function( node ) {
                if(node.name == "up" || node.name == "down"){
                    node.material = matY1;
                }
                if(node.name == "heart"){
                    node.material = matH;
                }
            });
            scene.add( heart );
            
            window.heart = heart;
            addParticle();
        },
        // called while loading is progressing
        function ( xhr ) {
            console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
        },
        // called when loading has errors
        function ( error ) {
            console.log( 'An error happened' );
        }
    );
}

function gltfLoaderBG(){
    // Instantiate a loader
    var loader = new THREE.GLTFLoader();

    // Load a glTF resource
    loader.load(
        // resource URL
        './bg.gltf',
        // called when the resource is loaded
        function ( gltf ) {
            gltf.scene.position.set(0, -1.1, 8);
            gltf.scene.scale.set(5, 5, 5);
            let object = gltf.scene;
            let matBG = new THREE.MeshPhongMaterial({color: 0xEEEEEE});
            let matStar = new THREE.MeshPhongMaterial({color: 0x3CDED3});

            object.traverse( function( node ) {
                if( node.name.toString().indexOf("star") != -1 ){
                    node.material = matStar;
                }

                if(node.name == "bg"){
                    node.material = matBG;
                }
                
            });
            scene.add( object );
            window.bg = object;
        },
        // called while loading is progressing
        function ( xhr ) {
            console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
        },
        // called when loading has errors
        function ( error ) {
            console.log( 'An error happened' );
        }
    );
}




function followMouse(){

    var vector = new THREE.Vector3(mouse.x, mouse.y, 0.5);
    vector.unproject( camera );
    var dir = vector.sub( camera.position ).normalize();
    var distance = - camera.position.z / dir.z;
    var pos = camera.position.clone().add( dir.multiplyScalar( distance ) );
}

let speed = minSpeed;
let particleSys = null;

function animate(){
    if(window.heart){

        if(isSpeedUp && speed<maxSpeed){
            speed += 0.005;
            if(speed >= maxSpeed){
                speed = maxSpeed;
            }
        }else{
            speed -= 0.02;
            if(speed<= minSpeed){
                speed = minSpeed;
            }
        }

        window.heart.rotation.y += speed;
    }

    if(particleSys){
        // let _v = particleSys.geometry.vertices;
        // for(let i=0; i< _v.length; i++){
        //     _v[i].x += 0.01 + i*0.01;
        //     if (_v[i].x >2) {
        //         _v[i].x = 0
        //     }
        // }

        particleSys.geometry.vertices.forEach(particle => {
            if(particle.alive){
                let mount = 30;
                particle.x += particle.targetVector.x/mount;
                particle.y += particle.targetVector.y/mount;
                particle.z += particle.targetVector.z/mount;
                if(particle.z >= 0.8){
                    particle.alive = false;
                    particle.x = 0;
                    particle.y = 0;
                    particle.z = 0;
                }
            }
            
        })
        particleSys.geometry.verticesNeedUpdate = true;
    }

    renderer.render( scene, camera );
    requestAnimationFrame( animate );
}


function addPostPass(){
    renderScene = new THREE.RenderPass(scene, camera);
    
    let effectFXAA = new THREE.ShaderPass(THREE.FXAAShader);
    effectFXAA.uniforms['resolution'].value.set(0.2 / window.innerWidth, 0.2 / window.innerHeight);
    
    let copyShader = new THREE.ShaderPass(THREE.CopyShader);
    copyShader.renderToScreen = true;

    let bloomPass = new THREE.UnrealBloomPass(new THREE.Vector2(window.innerWidth, window.innerHeight), params.bloomStrength, params.bloomRadius, params.bloomThreshold);
    bloomPass.renderToScreen = true;
    bloomPass.threshold = params.bloomThreshold;
    bloomPass.strength = params.bloomStrength;
    bloomPass.radius = params.bloomRadius;

    // glitchPass = new THREE.GlitchPass();


    composer = new THREE.EffectComposer(renderer);
    
    composer.setSize($('.container').width(), $('.container').height());
    composer.addPass(renderScene);
    composer.addPass(effectFXAA);
    composer.addPass(bloomPass);
    composer.addPass(copyShader);
    // composer.addPass(glitchPass);
}


let particleCount = 100;
let particleDistance = 53;
let particleGeom;
let particleMaterial;



function addParticle(){
    console.log("生成例子");
    particleGeom = new THREE.Geometry();

    particleMaterial = new THREE.PointsMaterial({
        color: 'rgb(255, 255, 255)',
        size: 40,
        map: new THREE.TextureLoader().load('./littleHeart.png'),
        transparent: true,
        // blending: THREE.AdditiveBlending,
    });

    
    for (let i=0; i<particleCount; i++) {

        //生成目標位置的隨機 2d 圓形區域
        let r, a, posY, posZ, M=0, R=0.3;
        r = R * Math.random();
        a = 2 * Math.PI * Math.random();

        posZ = r * Math.cos(a) + M;
        posY = r * Math.sin(a) + M;

        let particle = new THREE.Vector3(0, 0, 0);
        
        particle.targetVector = new THREE.Vector3(posZ, posY, 0.6);
        particle.targetVector = particle.targetVector.normalize();
        // particle.id = i;
        particleGeom.vertices.push(particle);

        console.log("加入一個點了");
    }


    setInterval(function() {
        if(isSpeedUp){
            addOneHeart();
        }
    }, 50);

    particleSys = new THREE.Points(particleGeom, particleMaterial);
    particleSys.name = 'particleSys';
    particleSys.position.set(0, 0.65, 0);
    window.ps = particleSys;
    heart.add(particleSys);
}

let useIndex = 0;
function addOneHeart(){
    //一次調整一個點
    particleSys.geometry.vertices[useIndex].alive = true;
    particleSys.geometry.verticesNeedUpdate = true;
    useIndex++;
    useIndex = useIndex%particleCount;
}


