let baseUrl = location.href;

// module aliases
let Engine = Matter.Engine,
    Render = Matter.Render,
    World = Matter.World,
    Bodies = Matter.Bodies;
let engine;
let boxs = [];
let ellipses = [];
let walls = [];

let env = {
    width: 500,
    height: 700
}

let straw;

function setup(){
    straw = loadImage(baseUrl+"/../s.png");
    createCanvas(env.width, env.height);

    engine = Engine.create();
    console.log(width, height);
    

    let wallThick = 5;

    walls.push(new Wall(wallThick/2+width/3, height/2, wallThick, height/3) );
    walls.push(new Wall(width-wallThick/2-width/3, height/2, wallThick, height/3) );
    walls.push(new Wall(width/2, height*0.67-wallThick/2, width/3, wallThick) );
    Engine.run(engine);

    noStroke();
}


function draw(){

    let mouseOffsetX = (width/2 - mouseX)/10;

    background(255);
    fill("#EDBA8B");
    rectMode(CENTER);
    rect(width/2-mouseOffsetX, height/2, 140, 210);
    
    image(straw, width/2, height/4);

    for(let i=0; i<boxs.length; i++){
        boxs[i].show();
    }

    for(let i=0; i<ellipses.length; i++){
        ellipses[i].show();
    }

    for(let i=0; i<walls.length; i++){
        walls[i].show(); 
        walls[i].offset( mouseOffsetX )
    }


    
}


function mousePressed(){
    ellipses.push( new Circle(mouseX, mouseY, 30, 30) );
}

function mouseDragged(){
    
}


function Box(x, y, w, h){
    this.body = Bodies.rectangle(x, y, w, h);
    this.w = w;
    this.h = h;
    World.add(engine.world, this.body);

    this.show = function(){
        let pos = this.body.position;
        let angle = this.body.angle;
        
        push();
        rectMode(CENTER);
        translate(pos.x, pos.y);
        rotate(angle);
        rect(0, 0, this.w, this.h);
        pop();
    }
}

function Circle(x, y, w, h){
    let opstion = {
        friction: 1,
        frictionAir: 0.5,
        restitution: 0.1,
        density: 0.01
    };
    this.body = Bodies.rectangle(x, y, w, h, opstion);
    this.w = w;
    this.h = h;
    World.add(engine.world, this.body);

    this.show = function(){
        let pos = this.body.position;
        let angle = this.body.angle;
        
        push();
        translate(pos.x, pos.y);
        rotate(angle);
        fill("#1C0000");
        ellipse(0, 0, this.w, this.h);
        fill("#300000");
        ellipse(0, 0, this.w*0.8, this.h*0.8);
        // rect(0, 0, this.w, this.h);
        pop();

    }
}

function Wall(x, y, w, h){

    this.body = Bodies.rectangle(x, y, w*2, h, { isStatic: true });
    this.w = w;
    this.h = h;
    World.add(engine.world, this.body);

    this.show = function(){
        let pos = this.body.position;
        let angle = this.body.angle;
        push();
        
        fill("#9A6363");
        rectMode(CENTER);
        translate(pos.x, pos.y);
        rotate(angle);
        rect(0, 0, this.w, this.h);
        pop();
    }

    this.offset = function(offset){
        this.body.position.x = x - offset;
    }
}