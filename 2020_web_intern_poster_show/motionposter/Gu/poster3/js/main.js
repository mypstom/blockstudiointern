


$(document).ready(function(){
    let ratioX = (1/768)*window.innerWidth;
    let ratioY = (1/1024)*window.innerHeight;
    TweenMax.fromTo("#c1", 8, {x: -23*ratioX, y: -53*ratioY}, {x: 777*ratioX, y: 753*ratioY, yoyo: 0, repeat: -1, ease: Linear.easeNone});
    TweenMax.fromTo("#c2", 6, {x: -30*ratioX, y: 167*ratioY}, {x: 769*ratioX, y: 970*ratioY, yoyo: 0, repeat: -1, ease: Linear.easeNone});
    TweenMax.fromTo("#c3", 8, {x: -43*ratioX, y: 470*ratioY}, {x: 526*ratioX, y: 1041*ratioY, yoyo: 0, repeat: -1, ease: Linear.easeNone});
    TweenMax.fromTo("#c4", 9, {x: 360*ratioX, y: -49*ratioY}, {x: 789*ratioX, y: 381*ratioY, yoyo: 0, repeat: -1, ease: Linear.easeNone});
    TweenMax.fromTo("#c5", 8, {x: 533*ratioX, y: -34*ratioY}, {x: -53*ratioX, y: 549*ratioY, yoyo: 0, repeat: -1, ease: Linear.easeNone});
    TweenMax.fromTo("#c6", 10, {x: 785*ratioX, y: 469*ratioY}, {x: 204*ratioX, y: 1049*ratioY, yoyo: 0, repeat: -1, ease: Linear.easeNone});
    TweenMax.fromTo("#c7", 4, {x: -45*ratioX, y: 728*ratioY}, {x: 800*ratioX, y: 728*ratioY, yoyo: 0, repeat: -1, ease: Linear.easeNone});
    TweenMax.fromTo("#c8", 11, {x: -45*ratioX, y: 120*ratioY}, {x: 800*ratioX, y: 120*ratioY, yoyo: 0, repeat: -1, ease: Linear.easeNone});
    // tick();

    
});

$( ".container" ).mousemove(function( event ) {
    var msg = "Handler for .mousemove() called at ";
    msg += event.pageX + ", " + event.pageY;
    // $(".mouse-circle").css("top", event.pageY-75);
    // $(".mouse-circle").css("left", event.pageX-75);
    // $(".mouse-circle").css("transform", `translateY(${event.pageY}px) translateX(${event.pageX}px)`);
});

let isToggle = false;
$( ".container" ).on("click", function() {
    //覆蓋滿整個畫面
    if(isToggle){
        isToggle = false;
        TweenMax.fromTo(".mouse-circle", 0.6, {width: 1500, height: 1500, x:"-50%", y: "-50%"}, {width: 0, height: 0, x:"-50%", y: "-50%"});
    }else{
        isToggle = true;
        TweenMax.fromTo(".mouse-circle", 0.6, {width: 0, height: 0, x:"-50%", y: "-50%"}, {width: 1500, height: 1500, x:"-50%", y: "-50%"});
    }
    
});


function tick(){
    let currentDistance = window.innerWidth/20;
    for(let i=1; i<9; i++){
        for(let j=1; j<9; j++){
            if(i==j) continue;
            let e1X = $('#c'+i ).position().left;
            let e1Y = $('#c'+i ).position().top;

            let e2X = $('#c'+j ).position().left;
            let e2Y = $('#c'+j ).position().top;

            if( Math.abs(e1X-e2X) < currentDistance && Math.abs(e1Y-e2Y) < currentDistance ){
                // $('#c'+j ).css("background-color", "white");
                // $('#c'+i ).css("background-color", "white");
                let tl = new TimelineMax();
                tl.fromTo(".effect", 0.3, {opacity: 0}, {opacity: 1});
                tl.fromTo(".effect", 0.3, {opacity: 1}, {opacity: 0});

                TweenMax.to(".collideEffect", 0.3, {x:(e1X+e2X)/2 -100  ,y:(e1Y+e2Y)/2-100})

                // $(".collideEffect").css("top", (e1Y+e2Y)/2+"px");
                // $(".collideEffect").css("left", (e1X+e2X)/2+"px");
                // console.log(e1X+e2X/2, e1Y+e2Y/2);
            }else{
                // $('#c'+j ).css("background-color", "#FE91CA");
                // $('#c'+i ).css("background-color", "#FE91CA");
            }
        
        }   
    }
    requestAnimationFrame(tick);
}