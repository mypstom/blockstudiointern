
let scene, camera, controls, clock, mixer=null;
let renderScene, bloomPass, composer;
let sphereSparkleState = false;
let blackAni, glassRefMat;

let params = {
    bloomStrength: 0.1,
    bloomThreshold: 0.5,
    bloomRadius: 0.2
};

let baby, baby2, bab3;
let isSpeedUp = false;
let maxSpeed = 1, minSpeed = 0.01;
let mamaMat, mamaText;

$(document).ready(()=>{
    setup();
    animate();
});

let bbkingTextTl;

function setup(){
    
    scene = new THREE.Scene();
    clock = new THREE.Clock();
    
    camera = new THREE.PerspectiveCamera( 60, $('.container').width()/ $('.container').height(), 0.1, 10000 );
    let width = 7.68, height = 10.24;
    // camera = new THREE.OrthographicCamera( width / - 2, width / 2, height / 2, height / - 2, 1, 10000 );
    // camera.rotation.y = -10;
    camera.position.set(0, 0, 15);
    
    if(isMobile()){
        renderer = new THREE.WebGLRenderer({alpha:true, antialias:true});
    }else{
        renderer = new THREE.WebGLRenderer({alpha:true, antialias:true, precision:"highp"});
    }

    renderer.setSize( $('.container').width(), $('.container').height() );
    // renderer.setClearColor( 0x060810, 0); 
    renderer.setPixelRatio( window.devicePixelRatio );
    $('.container').append( renderer.domElement )

    //controls = new THREE.OrbitControls(camera, renderer.domElement);

    // var helper = new THREE.GridHelper( 1000, 1000, 0x4411AA, 0x4411AA );
    // scene.add( helper );

    scene.background = new THREE.TextureLoader().load( "./bg.png" );



    //光照反射區
    /*
    var path = "./assets/cube/solid/";
    var format = '.jpg';
    var urls = [
        path + 'nx' + format, path + 'px' + format,
        path + 'ny' + format, path + 'py' + format,
        path + 'nz' + format, path + 'pz' + format
    ];

    var textureCube = new THREE.CubeTextureLoader().load( urls );

    textureCube.mapping = THREE.CubeRefractionMapping;
    // scene.background = textureCube;

    var shader = THREE.FresnelShader;
    var uniforms = THREE.UniformsUtils.clone( shader.uniforms );

    uniforms[ "tCube" ].value = textureCube;

    glassRefMat = new THREE.ShaderMaterial( {
        uniforms: uniforms,
        vertexShader: shader.vertexShader,
        fragmentShader: shader.fragmentShader
    } );
    */



    //環境光照
    var directionalLight = new THREE.DirectionalLight( 0xffffff, 1 );
    scene.add( directionalLight );
    var light = new THREE.AmbientLight( 0xFFFFFF ); // soft white light
    scene.add( light );
    
    
    loadModels();
    addPostPass();

    $( ".container" ).mousemove(function( event ) {
        var msg = "Handler for .mousemove() called at ";
        msg += event.pageX + ", " + event.pageY;
    });


    // $( "body" ).mousedown(function( event ) {
    //     console.log("down");
    //     isSpeedUp = true;
    // });

    // $( "body" ).mouseup(function( event ) {
    //     console.log("up");
    //     isSpeedUp = false;
    // });

    $( "body" ).on("click", function( event ) {
        // isSpeedUp = !isSpeedUp;
        if(mamaText.position.y<0){
            TweenMax.to(mamaText.position, 0.6, {repeat: 0, yoyo: true, y: 0});
        }else{
            TweenMax.to(mamaText.position, 0.6, {repeat: 0, yoyo: true, y: -25});
        }
    });

    // $(".touch").on("click", function(){
    //     if(mamaText.position.y<0){
    //         TweenMax.to(mamaText.position, 0.6, {repeat: 0, yoyo: true, y: 0});
    //     }else{
    //         TweenMax.to(mamaText.position, 0.6, {repeat: 0, yoyo: true, y: -25});
    //     }
    // });

    $(window).blur(function(){
        console.log("blur");
        isSpeedUp = false;
    });
    
}


function loadModels(){
    gltfLoaderBaby();
    addText();
    addMAMA();
}

function addMAMA(){
    mamaMat = new THREE.ShaderMaterial({
        vertexShader: $("#vertexShader").text(),
        fragmentShader: $("#fragmentShader").text(),
        uniforms: {
            uTime: { 
              value: 0.0,
            },
            uTexture: { 
                value: new THREE.TextureLoader().load("./bbking.png")
            },
            percent:{
                percent: { type: "f", value: 1.0 }
            }
        },
        // wireframe: true,
        transparent: true,
        side: THREE.DoubleSide
    });

    let geometry = new THREE.PlaneBufferGeometry(3.67*4.2, 4.51*4.8, 32, 32);
    let mesh = new THREE.Mesh( geometry, mamaMat );
    mesh.position.set(0, -25, -5);
    scene.add( mesh );
    mamaText = mesh;
    

    // let sprite = new THREE.Sprite( mamaMat );
    // sprite.scale.set(3.67*2, 4.51*2);
    // sprite.position.z = -2;
    // sprite.position.x = 3.6;
    // sprite.position.y = 2.9;
    // scene.add( sprite );
}


function addText(){
    //載入文字圖片
    let screamMat = new THREE.MeshBasicMaterial( { 
        map: new THREE.TextureLoader().load( "./SCREAMMING.png" ),
        color: 0xffffff,
        transparent: true
        // blending: THREE.AdditiveBlending
    } );

    for(let i=0; i<6; i++){
        let sprite = new THREE.Sprite( screamMat );
        sprite.scale.set(120/3.0, 7/3.0);
        sprite.position.z = -2;
        sprite.position.x = -13.5 + i;
        sprite.position.y = -1.6*(i) - 1;
        TweenMax.to(sprite.position, 3 + i, {x: 0 + i, yoyo: false, repeat: -1, ease: Power0.easeNone});
        scene.add( sprite );
    }
}


function gltfLoaderBaby(){
    // Instantiate a loader
    var loader = new THREE.GLTFLoader();

    // Load a glTF resource
    loader.load(
        // resource URL
        './baby.gltf',
        // called when the resource is loaded
        function ( gltf ) {
            gltf.scene.position.set(-3, -7, 0);
            gltf.scene.scale.set(3, 3, 3);
            baby = gltf.scene;

            let material = new THREE.MeshStandardMaterial({
                // map: new THREE.TextureLoader().load( "./water.jpg" ),
                color: 0x223333,  // 物件顏色
                blending: THREE.AdditiveBlending,
                transparent: true,
                opacity: 0.5
            })

            baby.children[0].traverse( function( node ) {
                if(node.material){
                    // node.material = glassRefMat;
                    node.material = material;
                }
            });
            scene.add(baby);

            //加入其他卑鄙
            // baby2 = baby.clone();
            // baby2.position.set(-5, -2.5, 0);
            // scene.add(baby2);
            // baby3 = baby.clone();
            // baby3.position.set(5, -2.5, 0);
            // scene.add(baby3);


            outlinePass.selectedObjects = [];
            outlinePass.selectedObjects.push(baby.children[0]);
            // outlinePass.selectedObjects.push(baby2.children[0]);
            // outlinePass.selectedObjects.push(baby3.children[0]);
            window.baby = baby;
        },
        // called while loading is progressing
        function ( xhr ) {
            console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
        },
        // called when loading has errors
        function ( error ) {
            console.log( 'An error happened' );
        }
    );
}


let speed = minSpeed;
let particleSys = null;

function animate(){
    if(baby){
        baby.rotation.y -=0.05;
        // baby2.rotation.y +=0.1;
        // baby3.rotation.y +=0.1;
    }
    if(mamaMat && baby){
        mamaMat.uniforms.uTime.value = baby.rotation.y;
    }
    renderer.render( scene, camera );
    composer.render();
    requestAnimationFrame( animate );
}



let outlinePass;

function addPostPass(){
    renderScene = new THREE.RenderPass(scene, camera);
    
    let effectFXAA = new THREE.ShaderPass(THREE.FXAAShader);
    effectFXAA.uniforms['resolution'].value.set(0.2 / window.innerWidth, 0.2 / window.innerHeight);
    
    let copyShader = new THREE.ShaderPass(THREE.CopyShader);
    copyShader.renderToScreen = true;

    let bloomPass = new THREE.UnrealBloomPass(new THREE.Vector2(window.innerWidth, window.innerHeight), params.bloomStrength, params.bloomRadius, params.bloomThreshold);
    bloomPass.renderToScreen = true;
    bloomPass.threshold = params.bloomThreshold;
    bloomPass.strength = params.bloomStrength;
    bloomPass.radius = params.bloomRadius;

    outlinePass = new THREE.OutlinePass( new THREE.Vector2( window.innerWidth, window.innerHeight ), scene, camera );
    outlinePass.edgeStrength = 20.0;
    outlinePass.edgeGlow = 0;
    outlinePass.edgeThickness = 10.0;
    outlinePass.pulsePeriod = 0;
    outlinePass.rotate = false;
    outlinePass.usePatternTexture = false;
    outlinePass.visibleEdgeColor = new THREE.Color(0xFFDFDF22);

    composer = new THREE.EffectComposer(renderer);
    
    composer.setSize($('.container').width(), $('.container').height());
    composer.addPass(renderScene);
    composer.addPass(effectFXAA);
    composer.addPass(bloomPass);
    composer.addPass(copyShader);
    composer.addPass( outlinePass );

    
}

