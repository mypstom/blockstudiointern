let baseUrl = "./";

// module aliases
let Engine = Matter.Engine,
    Render = Matter.Render,
    World = Matter.World,
    Bodies = Matter.Bodies,
    Body = Matter.Body,
    Composites = Matter.Composites,
    Events = Matter.Events,
    Mouse = Matter.Mouse,
    MouseConstraint = Matter.MouseConstraint;

let engine;
let boxs = [];
let ellipses = [];
let walls = [];



let env = {
    width: window.innerWidth,
    height: window.innerHeight
}

let cradle;
let label1, label2, label3;
let mouseConstraint;
let soundGen;

let r, g, b, a;

let ratioX = (1/768)*window.innerWidth;
let ratioY = (1/1024)*window.innerHeight;

function setup(){
    pixelDensity(3.0);
    createCanvas(env.width, env.height);
    engine = Engine.create();
    Engine.run(engine);


    //文字標籤
    label1 = loadImage(baseUrl+"/images/l1.png");
    label2 = loadImage(baseUrl+"/images/c1.png");
    label3 = loadImage(baseUrl+"/images/r1.png");

    //牛頓撞撞球
    cradle = Composites.newtonsCradle(200*ratioX, 450*ratioY, 4, 91*ratioX, 200*ratioY);
    World.add(engine.world, cradle);
    Body.translate(cradle.bodies[0], { x: -180*ratioX, y: -100*ratioY });
    console.log(cradle);

    //撞撞球顯示
    for(let i=0; i<4; i++){
        ellipses.push(new Circle(0, 0, 180*ratioX, 180*ratioX, i));
    }


    //動態聲音文字產生器
    soundGen = new soundTextGenerator();
    soundGen.setup();

    //碰撞偵測
    Events.on(engine, 'collisionStart', function(event) {
        var pairs = event.pairs;
        for (var i = 0; i < pairs.length; i++) {
            var pair = pairs[i];

            let centerPos = {
                x: (pair.bodyA.position.x + pair.bodyB.position.x)/2,
                y: (pair.bodyA.position.y + pair.bodyB.position.y)/2,
            }

            soundGen.makeSound(centerPos);

            ellipses[(pair.bodyA.id)/2-1].collisionTime = frameCount;
            ellipses[(pair.bodyB.id)/2-1].collisionTime = frameCount;

        }
        getRandomColor();
    });


    //滑鼠影響力
    mouseConstraint = MouseConstraint.create(engine, {
        constraint: {
            stiffness: 1,
            render: {
                visible: true
            }
        }
    })

    World.add(engine.world, mouseConstraint);

}


function draw(){
    // scale(0.39);
    background(r, g, b, a);
    noFill();
    stroke(180);
    for(let i=0; i<5; i++){
        // ellipse(cradle.bodies[i].position.x, cradle.bodies[i].position.y, _r*2, _r*2);
    }
    
    for(let i=0; i<ellipses.length; i++){
        let _r = cradle.bodies[i].circleRadius;
        ellipses[i].position(cradle.bodies[i].position.x, cradle.bodies[i].position.y);
        ellipses[i].show();
    }

    soundGen.show();

    //左
    image(label1, 50*ratioX, 800*ratioY, label1.width/2*ratioX, label1.height/2*ratioY);
    //中
    image(label2, 250*ratioX, 700*ratioY, label2.width/2*ratioX, label2.height/2*ratioY);
    //右
    image(label3, 650*ratioX, 80*ratioY, label3.width/2*ratioX, label3.height/2*ratioY);


    if(frameCount%1600 == 0 && false){
        //沒力量了補一點
        Body.translate(cradle.bodies[0], { x: -180*ratioX, y: -100*ratioY });
    }
    
}


function mousePressed(){
    //沒力量了補一點
    Body.translate(cradle.bodies[0], { x: -180*ratioX, y: -100*ratioY });
}

function mouseDragged(){
    
}



function getRandomColor(){
    r = random(255); // r is a random number between 0 - 255
    g = random(100,200); // g is a random number betwen 100 - 200
    b = random(100, 255); // b is a random number between 0 - 100
    a = 255;
}



function Circle(x, y, w, h, id){
    //顯示球球用的
    this.id = id;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.fillColor = "#6ebfb5";
    this.collisionTime = 0;
    
    this.position = function(x, y){
        this.x = x;
        this.y = y;
    }

    this.fill = function(color){
        this.fillColor = color;
    }

    this.show = function(){
        stroke("#6ebfb5");
        strokeWeight(2);
        noFill();

        line((this.id*130+200)*ratioX, 0, this.x, this.y);
        
        push();
        translate(this.x, this.y);
        // rotate(angle);
        strokeWeight(4);
        if( abs(this.collisionTime - frameCount) < 10){
            fill("#d9455f");
        }else{
            fill("#6ebfb5");
        }
        ellipse(0, 0, this.w, this.h);
        pop();
        
    }
}


function soundTextGenerator(){

    this.srcGroup = [];
    this.textGroup = [];
    

    this.setup = function(){
        for(let i=0; i<4; i++){
            this.srcGroup.push(loadImage(baseUrl+"/../images/sound"+ i +".png"));
        }
    }

    this.makeSound = function(pos){
        this.textGroup.push( new soundText(pos.x, pos.y, this.srcGroup[floor(random(4))]) );
    }
    
    this.show = function(){
        for(let i=0; i<this.textGroup.length; i++){
            this.textGroup[i].show();
        }
        if(frameCount%500 == 0){
            this.textGroup.shift();
        }
    }

}


function soundText(x, y, src){
    this.x = x;
    this.y = y;
    this.src = src;
    this.scale = random(0.2*ratioX, 2*ratioX);
    this.moveVector = p5.Vector.fromAngle( radians(random(0, 360)) , 1);

    this.show = function(){
        push();
        imageMode(CENTER);
        translate(this.x, this.y);
        scale(this.scale/10);
        image(this.src, 0, 0);
        pop();

        this.x += this.moveVector.x;
        this.y += this.moveVector.y;
    }
}


function windowResized() {
  resizeCanvas(window.innerWidth, window.innerHeight);
}